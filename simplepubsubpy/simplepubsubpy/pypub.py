import rclpy
from rclpy.node import Node

from std_msgs.msg import Int16

class TicksPublisher(Node):

    def __init__(self):
        super().__init__('ticks_publisher')
        self.ltickspub_ = self.create_publisher(Int16, 'lticks', 10)
        self.timer1 = self.create_timer(0.5, self.lticks_callback)
        self.rtickspub_ = self.create_publisher(Int16, 'rticks', 10)
        self.timer2 = self.create_timer(0.5, self.rticks_callback)
        

    def lticks_callback(self):
        msg = Int16()
        msg.data = 0
        self.ltickspub_.publish(msg)
        # self.get_logger().info('Publishing: "%s"' % msg.data)

    def rticks_callback(self):
        msg = Int16()
        msg.data = 0
        self.rtickspub_.publish(msg)
        


def main(args=None):
    rclpy.init(args=args)
    ticks_publisher = TicksPublisher()
    rclpy.spin(ticks_publisher)
    ticks_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()